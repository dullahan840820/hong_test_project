package word

import (
	"strings"
	"unicode"
)

// ToLower : 將取得單詞轉為全大寫
func ToUpper(s string) string {
	return strings.ToUpper(s)
}

// ToLower : 將取得單詞轉為全小寫
func ToLower(s string) string {
	return strings.ToLower(s)
}

// UnderscoreToUpperCamelCase : 將下劃線的單詞轉為大寫駝峰
func UnderscoreToUpperCamelCase(s string) string {
	s = strings.Replace(s, "_", "", -1)
	s = strings.Title(s)
	return strings.Replace(s, " ", "", -1)
}

// UnderscoreToLowerCamelCase :將下劃線的單詞轉為小寫駝峰
func UnderscoreToLowerCamelCase(s string) string {
	s = UnderscoreToUpperCamelCase(s)
	return string(unicode.ToLower(rune(s[0]))) + s[1:]
}

// CamelCaseToUnderscore : 駝峰單詞轉下劃線單詞
func CamelCaseToUnderscore(s string) string {
	var output []rune
	for i, r := range s {
		if i == 0 {
			output = append(output, unicode.ToLower(r))
			continue
		}
		if unicode.IsUpper(r) {
			output = append(output, '_')
		}
		output = append(output, unicode.ToLower(r))
	}
	return string(output)
}
